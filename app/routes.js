const { exchangeRates } = require('../src/util.js');
const express = require("express");
const router = express.Router();

	router.get('/rates', (req, res) => {
		return res.status(200).send(exchangeRates);
	})

// SEE FINAL SOLUTION @ REFACTORED CODE

	// router.post('/currency', (req, res) => {
	// 	if(!req.body.hasOwnProperty("name")){
	// 		return res.status(400).send({
	// 			error: "Bad Request - missing required parameter NAME"
	// 		})
	// 	}
	// 	if(typeof req.body.name !== "string"){
	// 		return res.status(400).send({
	// 			error: "Bad Request - NAME has to be a string"
	// 		})
	// 	}
	// 	if(req.body.name == ""){
	// 		return res.status(400).send({
	// 			error: "Bad Request - NAME should not be empty"
	// 		})
	// 	}
	// 	if(!req.body.hasOwnProperty("ex")){
	// 		return res.status(400).send({
	// 			error: "Bad Request - missing required parameter EX"
	// 		})
	// 	}
	// 	if(typeof req.body.ex !== "object"){
	// 		return res.status(400).send({
	// 			error: "Bad Request - EX has to be an object"
	// 		})
	// 	}
	// 	if(Object.keys(req.body.ex).length === 0){
	// 		return res.status(400).send({
	// 			error: "Bad Request - EX should be not empty"
	// 		})
	// 	}
	// 	if(!req.body.hasOwnProperty("alias")){
	// 		return res.status(400).send({
	// 			error: "Bad Request - missing required parameter ALIAS"
	// 		})
	// 	}
	// 	if(typeof req.body.alias !== "string"){
	// 		return res.status(400).send({
	// 			error: "Bad Request - ALIAS has to be a string"
	// 		})
	// 	}
	// 	if(req.body.alias == ""){
	// 		return res.status(400).send({
	// 			error: "Bad Request - ALIAS should not be empty"
	// 		})
	// 	}

	// 	let foundExchangeRates = exchangeRates.find((rate) => {
	// 		return rate.alias === req.body.alias &&
	// 			rate.name !== "" && 
	// 			rate.ex !== Object.keys({}).length
	// 	});

	// 	if(foundExchangeRates){
	// 		return res.status(400).send({
	// 			error: "Bad Request: duplicate credentials"
	// 		})
	// 	} else {
	// 		return res.status(200).send({
	// 			message: "All fields are complete and no duplicates"
	// 		})
	// 	}
	// })

	// REFACTORED CODE

	router.post('/currency', (req, res) => {
		if(!req.body.hasOwnProperty("name") || !req.body.hasOwnProperty("ex") || !req.body.hasOwnProperty("alias")){
			return res.status(400).send({
				error: "Bad Request - missing required parameter ALIAS or NAME or EX "
			})
		}
		if(typeof req.body.name !== "string" || typeof req.body.alias !== "string"){
			return res.status(400).send({
				error: "Bad Request - NAME and ALIAS has to be a string"
			})
		}
		if(req.body.name == "" || req.body.alias == ""){
			return res.status(400).send({
				error: "Bad Request - NAME and ALIAS should not be empty"
			})
		}
		// object conditions
		if(typeof req.body.ex !== "object"){
			return res.status(400).send({
				error: "Bad Request - EX has to be an object"
			})
		}
		if(Object.keys(req.body.ex).length === 0){
			return res.status(400).send({
				error: "Bad Request - EX should not be empty"
			})
		}

		let foundExchangeRates = exchangeRates.find((rate) => {
			return rate.alias === req.body.alias &&
				rate.name !== "" && 
				rate.ex !== Object.keys({}).length
		});

		if(foundExchangeRates){
			return res.status(400).send({
				error: "Bad Request: duplicate credentials"
			})
		} else {
			return res.status(200).send({
				message: "All fields are complete and no duplicates"
			})
		}
	})



module.exports = router;
