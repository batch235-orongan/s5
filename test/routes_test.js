const chai = require('chai');
const{ assert }= require('chai');
const http = require('chai-http');
chai.use(http);

describe('forex_api_test_suite', () => {
	
// NOT INCLUDED IN THE ACTIVITY	
	// it('test_api_get_rates_is_running', (done) => {
	// 	chai.request('http://localhost:5001').get('/forex/rates')
	// 	.end((err, res) => {
	// 		assert.isDefined(res);
	// 		done();
	// 	})
	// })
	
	// it('test_api_get_rates_returns_200', (done) => {
	// 	chai.request('http://localhost:5001')
	// 	.get('/forex/rates')
	// 	.end((err, res) => {
	// 		assert.equal(res.status,200)
	// 		done();	
	// 	})		
	// })
	// 1. 
	it("Test API post currency is running", () => {
		chai.request("http://localhost:5001").post("/forex/currency")
		.type("json")
		.send({
			alias: 'nuyen',
			name: 'Shadowrun Nuyen',
			ex: {
				'peso': 0.47,
		        'usd': 0.0092,
		        'won': 10.93,
		        'yuan': 0.065
			}
		})
		.end((err, res) => {
			assert.notEqual(res.status,400)
		
		})
	})
	// 2.
	it("Test API post currency returns 400 if name is missing", (done) => {
		chai.request("http://localhost:5001").post("/forex/currency")
		.type("json")
		.send({
			alias: 'nuyen',
			// name: 'Shadowrun Nuyen',
			ex: {
				'peso': 0.47,
		        'usd': 0.0092,
		        'won': 10.93,
		        'yuan': 0.065
			}
		})
		.end((err, res) => {
			assert.equal(res.status,400)
			done();
		})
	})
	// 3.
	it("Test API post currency returns 400 if name is not a string", (done) => {
		chai.request("http://localhost:5001").post("/forex/currency")
		.type("json")
		.send({
			alias: 'nuyen',
			name: 12345,
			ex: {
				'peso': 0.47,
		        'usd': 0.0092,
		        'won': 10.93,
		        'yuan': 0.065
			}
		})
		.end((err, res) => {
			assert.equal(res.status,400)
			done();
		})
	})
	// 4.
	it("Test API post currency returns 400 if name is empty string", (done) => {
		chai.request("http://localhost:5001").post("/forex/currency")
		.type("json")
		.send({
			alias: 'nuyen',
			name: "",
			ex: {
				'peso': 0.47,
		        'usd': 0.0092,
		        'won': 10.93,
		        'yuan': 0.065
			}
		})
		.end((err, res) => {
			assert.equal(res.status,400)
			done();
		})
	})
	// 5.
	it("Test API post currency returns 400 if ex is missing", (done) => {
		chai.request("http://localhost:5001").post("/forex/currency")
		.type("json")
		.send({
			alias: 'nuyen',
			name: 'Shadowrun Nuyen',
			// ex: {
			// 	'peso': 0.47,
		 //        'usd': 0.0092,
		 //        'won': 10.93,
		 //        'yuan': 0.065
			// }
		})
		.end((err, res) => {
			assert.equal(res.status,400)
			done();
		})
	})
	// 6.
	it("Test API post currency returns 400 if ex is not an object", (done) => {
		chai.request("http://localhost:5001").post("/forex/currency")
		.type("json")
		.send({
			alias: 'nuyen',
			name: 'Shadowrun Nuyen',
			ex: "I am a string"
		})
		.end((err, res) => {
			assert.equal(res.status,400)
			done();
		})
	})
	// 7.
	it("Test API post currency returns 400 if ex is an empty object", (done) => {
		chai.request("http://localhost:5001").post("/forex/currency")
		.type("json")
		.send({
			alias: 'nuyen',
			name: 'Shadowrun Nuyen',
			ex: {}
		})
		.end((err, res) => {
			assert.equal(res.status,400)
			done();
		})
	})
	// 8.
	it("Test API post currency returns 400 if alias is missing", (done) => {
		chai.request("http://localhost:5001").post("/forex/currency")
		.type("json")
		.send({
			// alias: 'nuyen',
			name: 'Shadowrun Nuyen',
			ex: {
				'peso': 0.47,
		        'usd': 0.0092,
		        'won': 10.93,
		        'yuan': 0.065
			}
		})
		.end((err, res) => {
			assert.equal(res.status,400)
			done();
		})
	})
	// 9.
	it("Test API post currency returns 400 if alias is not a string", (done) => {
		chai.request("http://localhost:5001").post("/forex/currency")
		.type("json")
		.send({
			alias: 12345,
			name: 'Shadowrun Nuyen',
			ex: {
				'peso': 0.47,
		        'usd': 0.0092,
		        'won': 10.93,
		        'yuan': 0.065
			}
		})
		.end((err, res) => {
			assert.equal(res.status,400)
			done();
		})
	})
	// 10.
	it("Test API post currency returns 400 if alias is empty string", (done) => {
		chai.request("http://localhost:5001").post("/forex/currency")
		.type("json")
		.send({
			alias: '',
			name: 'Shadowrun Nuyen',
			ex: {
				'peso': 0.47,
		        'usd': 0.0092,
		        'won': 10.93,
		        'yuan': 0.065
			}
		})
		.end((err, res) => {
			assert.equal(res.status,400)
			done();
		})
	})
	// 11.
	it("Test API post currency returns 400 if all fields are complete but there is a duplicate alias", (done) => {
		chai.request("http://localhost:5001").post("/forex/currency")
		.type("json")
		.send({
			alias: 'yen',
			name: 'Shadowrun Nuyen',
			ex: {
				'peso': 0.478,
		        'usd': 0.0093,
		        'won': 10.94,
		        'yuan': 0.066
			}
		})
		.end((err, res) => {
			assert.equal(res.status,400)
			done();
		})
	})
	// 12.
	it('test_api_post_currency_returns_200_if_complete_input_given', (done) => {
		chai.request('http://localhost:5001')
		.post('/forex/currency')
		.type('json')
		.send({
			alias: 'nuyen',
			name: 'Shadowrun Nuyen',
			ex: {
				'peso': 0.47,
		        'usd': 0.0092,
		        'won': 10.93,
		        'yuan': 0.065
			}
		})
		.end((err, res) => {
			assert.equal(res.status,200)
			done();
		})
	})


})
